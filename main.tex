\documentclass[a4paper]{jpconf}

\usepackage[smaller]{acronym}
\usepackage[inline]{enumitem}
\usepackage{graphicx}
\usepackage{siunitx}

\bibliographystyle{iopart-num}

\acrodef{APT}{Alan Cousins Automatic Photometric Telescope}
\acrodef{TOO}{target-of-opportunity}

\begin{document}

\title{Automated scheduling for a robotic astronomical telescope}
\author{D Maartens$^{1,2}$, P Martinez$^3$ and R van Rooyen$^4$}
\address{$^1$ Southern African Large Telescope, Observatory Road, Observatory, Cape Town 7935}
\address{$^2$ South African Astronomical Observatory, Observatory Road, Observatory, Cape Town 7935}
\address{$^3$ SpaceLab, University of Cape Town, Rondebosch, Cape Town 7700}
\address{$^4$ SKA South Africa, 3rd Floor, The Park, Park Road, Pinelands, 7405, Western Cape, South Africa}

\ead{dsm@salt.ac.za}

\begin{abstract}
Scheduling related to science instruments is typically more complex and quite different from the standard, application-related scheduling problems which are routinely solved in industry. For instance, the problem requires making choices which impact other choices later and contains many interacting complex constraints over both discrete and continuous variables. Furthermore, the set of applicable constraints is particular to a science project, while new types of constraints may be added as the fundamental problem changes.

The aim of this project is to design and develop a more general scheduling strategy to deal with the eccentricities of astronomy scheduling. Resulting on a strawman implementation of such an automated scheduler for optical photometers such as the \acf{APT}.
\end{abstract}

\section{Introduction}
The \acf{APT} is a 0.75-\si{\metre} automatic photoelectric telescope commissioned in mid-2000. The sole science driver for the \ac{APT} is photometry, used mainly for the long-term monitoring of variable stars. In addition, there is the potential for \ac{TOO} observations such as gamma ray bursts and solar eclipse observations. Ultimately the \ac{APT} is expected to be fully robotic. Some advance toward this goal has been made. The next phase is the development of an automated scheduler that will generate a pool of viable observations for each night of observation.% The \ac{APT} strawman scheduler will address the problem of distributing time on a single instrument.

Astronomy projects are complex, often consisting of inseparably connected constraints, requiring long-term planning as well as short-term optimisation~\cite{Frank2003}. While one aspect of scheduling focuses on optimising resource utilisation as the goal, conditions can change significantly during an observing session, leading to schedule breakage. In addition, science observing may require prompt follow-up observations that arise during an observation session. These issues give rise to the need for a scheduling system that is capable of recovering from periods of bad observational conditions and of integrating newly added observations during operation~\cite{GomezdeCastro2003}.

Planning and scheduling are distinctly different activities. Planning is not only concerned with setting up an observation plan for a telescope and\slash{}or instruments but it also entails planning, carried out by the observatory, on scheduling the observations to achieve some objective. Scheduling requires planning information to assess temporal assignment of observations to best achieve an execution plan. Observing has to deal with the dynamic conditions during execution of an observation and best choice selection based on available observation plans~\cite{Wall1996}.

Considering the three distinct time scales that fundamentally makes up astronomical scheduling, we will describe the problem using a design that contains three stages. Implementation of the stages as distinct modules are not always required, but all modules should incorporate the criteria for good scheduling.

\section{Scheduler Design}
The three criteria for a ``good schedule'' are \begin{enumerate*}[label=(\emph{\alph*})] \item fairness, \item efficiency, and \item sensibility.\end{enumerate*} A fair schedule balances time allocations between users such that they all share good and bad observing times equitably. An efficient schedule is one that maximises instrument utilisation and strives to match observations with required conditions. A sensible schedule is one that attempts only those observations that are possible under the current observing conditions~\cite{Denny2004}. Optimisation aims of the various criteria are represented through the stages of the design which are connected by an objective function representing a per observation rank calculation based on constraints. An observation specifies both hard constraints and soft preferences. The scheduling problem is to synthesise a schedule that satisfies all hard constraints and achieves a good score according to an objective function based on the soft preferences~\cite{Swanson1994}. 

\subsection{Planning}
Longer term planning deals with scheduling over the observation season, given the approved science projects. The main aim of this section is the fair distribution of time among users\slash partners, as well as maximising scientific return.

Optimisation for long term planning is mainly driven by the aims of the observatory and are restricted by the constraints of the telescope. Observations can, and usually do, conflict. Longer term plans allow for better resolution of these conflicts to achieve optimal scientific output.

Intermittent re-planning allows for the re-evaluation of observatory performance, which in turn allows for the re-evaluation of parameters or change of optimisation function. Furthermore, over the lifetime of the observatory, other constraints may be required such as new instrumentation or change in operations.

The optimisation strategy for planning ignores dynamic conditions and assumes \begin{enumerate*}[label=(\emph{\alph*})] \item problem-free execution of each observation, \item perfect knowledge of the time duration needed for each observation, and \item perfect fore-knowledge of the weather throughout the night.\end{enumerate*}

Given the complexity and size of the search space for long term scheduling, it is obvious that this type of scheduling cannot be done in real time, therefore the focus does not have to be on time-efficient algorithms.

\subsection{Scheduling}
This is the more traditional form of scheduling, focused on optimising the use of the observatory, minimising overhead and maximising science output.

Setting up a pool of observations available for execution based on a subset from the planning section allows for the efficient use of telescope time and instrumentation setup. While planning decreases the number of observations to consider based on best choice and other fixed constraints, setting up a selection of viable observations is composed of a large number of complex, heterogeneous constraints over both continuous and discrete variables. Even relatively simple schedules have to deal with geometric constraints, precedence constraints, mutual exclusion constraints and temporal constraints, all in the same problem~\cite{GomezdeCastro2003}.

Non-scorable observations are subject to their own unique scheduling rules. Such observations are fixed observations that must be scheduled at a specific date and time regardless of the possible score. The only goal here is to make certain everything actually gets on the schedule, given its individual constraints. Optimisation of other observations happens around these observations and will generally result in a less-optimal solution.

\subsection{Observations}
Executing observations are an extremely time-constrained activity, leaving little time for peripheral actions. Only minimal optimisation must be done in order to ensure efficiency and sensibility.

The scheduler can generate a pool of observations available to the executioner based on predicted values---duration of the night, observation plan, etc. The executioner must continually process short term viability based on constant updates that can include added observations, constraints, or weather conditions.

Given a set of observations to perform; a date and time to perform them, a description of the environment; and the objective function; select the observation that maximises the objective function and satisfies all the constraints.

\section{Constraint Evaluation using Merit Functions}
Operational parameters may be general to astronomy or unique to an observatory, instrument, observation mode, etc. Other influences will depend on observatory policies and procedures such as those related to long-term projects, or compensations for time loss due to \ac{TOO} observations or similar programs.

The most complex part of an observation request are the constraints on the observation. Some of these constraints are explicitly given by astronomers, while others are implicit, due to the nature of instrument\slash telescope~\cite{Frank2003}.

In order to decide which observation, $n$, to carry out, the rank is defined as~\cite{Steele1997}:
\begin{equation}
R(n) = f(n)\cdot \prod\limits_{x=1}^{x=X}\upsilon_x(n) \cdot \frac{\sum\limits_{m=1}^{m=M}\varepsilon_m(n)}{M}
\end{equation}
For any observation constrained by $X$ hard limits and $M$ soft preferences: $f(n)$ is a measure of fairness, $\varepsilon_m(n)$ measures of efficiency and $\upsilon_x(n)$ Boolean veto functions as measures of sensibility.

Observatory time must be shared equitably between projects. The fairness function evaluates how fair doing a particular observation is based on the project's time allowance. The time allocated by partners is thus a form of observatory accounting and when share values drop below a reasonable number, the system must give higher preference to that partner~\cite{Kubanek2008}.

The veto function has to prevent observations being carried out that are not possible at the current time due to a number of Boolean constraints.
\begin{equation}
\prod\limits_{x=1}^{x=X}\upsilon_x(n) = \upsilon_1(n)\cdot \upsilon_2(n)\cdot ...\cdot \upsilon_X(n) \, ,
\end{equation}
where $\upsilon_x(n)$ describes the constraint limits.

The purpose of the efficiency merits is to decide which observation to carry out at any given moment in time considering observatory policy, scientific expectation and observing conditions~\cite{Steele1997}.
\begin{equation}
\sum\limits_{m=1}^{m=M}\varepsilon_m(n) = \beta_1 \varepsilon_1(n) + \beta_2 \varepsilon_2(n) + ... + \beta_M \varepsilon_M(n) \, ,
\end{equation}
where $\varepsilon_m(n)$ describes the constraint equations, each with an optional weighting factor $\beta_m$.


\subsection{Astronomical Veto Functions}
Astronomical constraints that can be considered as ``hard'' constraints, are generally related to observational limits.

Target brightness evaluation is based on the instrument sensitivity limits related to the source target properties. The brightness of the object must be low enough not to saturate the instrument, but high enough to be a viable observation.
\begin{equation}
\upsilon(\mathit{magnitude}) = 1 \in [\mbox{noise limit, brightness limit}]
\end{equation}

Lunar phase and elevation not only influence sky brightness calculations but also determines a hard limit to observational ``brightness'' conditions and can be defined in terms of Lunar illumination as a percentage of the surface illumination (PLI).
\begin{equation}
\begin{array}{l}
\mbox{Lunar brightness} = \left\{
\begin{array}{l}
\mbox{dark},\, \mbox{if PLI} < 0.4 \\
\mbox{grey},\, \mbox{if PLI} < 0.7 \\
\mbox{no constraint},\, \mbox{True}
\end{array}
\right . \\
\hfill\\
\arraycolsep=1.8pt
\begin{array}{rcl}
\upsilon(\mathit{dark}) & = 1,\, & \mbox{if dark}\\
\upsilon(\mathit{grey}) & = 1,\, & \mbox{if dark} \cup \mbox{grey}\\
\upsilon(\mathit{any}) & = 1,\, & \mbox{if dark}^c \cap \mbox{grey}^c
\end{array}
\hfill\\
\end{array}
\end{equation}

It is only worthwhile to consider a target for observation if that target will be visible during the observation session. For an optical telescope, this means selecting a sequence of observations starting at some time after sunset, $N_{\mathit{start}}$, and ending before sunrise, $N_{\mathit{end}}$.
\begin{equation}
\upsilon(\mathit{visible}) = 1\, \forall\, h_{\mathit{target}>\mathit{horizon}} \in (N_{\mathit{start}}, N_{\mathit{end}})
\end{equation}

\subsection{Astronomical Efficiency Functions}
General constraints can be used to optimise the observation scheduling. The strictness of application of these ``soft'' preferences depends on the observation. To allow the flexibility and fuzziness that is required by astronomical observations, soft constraints are defined using merit functions using simple parameters that can be adjusted to make the constraint more, or less, stringent.

Airmass (Figure~\ref{fig:airmass}) can be used to assign lower weighting as the targets get closer to the horizon, thereby favouring observations at higher elevation:
\begin{equation}
\varepsilon_h(\mathit{airmass}) = \frac{1}{(z(h))^\alpha} \, ,
\end{equation}
for some observatory related airmass model, $z$.

The target must be a specific angular distance from the Moon. Separation angles may be dependent on the observation wavelength with different criteria between longer and shorter wavelengths, or brightness of target and comparator pair (Figure~\ref{fig:separation}). It is advised that the separation angle be chosen as narrow as possible since very strict phase and angle requirements may drastically reduce the time period in which the observation can be carried out, and hence a lower probability that it would be successfully completed.
\begin{equation}
\varepsilon(\mathit{separation}) = \left(\frac{\theta(\mathit{target}, \mathit{Moon})-a}{b}\right)^c
\end{equation}
For a separation distance $\theta$ and separation limit $a$. Parameters $b$ and $c$ is used to shape the strictness of the merit.
\begin{figure}[ht]
\centering
\begin{minipage}[t]{.49\textwidth}
  \centering
  \includegraphics[width=.75\linewidth]{images/airmass_merit}
  \caption{\label{fig:airmass}\small Using the homogeneous plane-parallel atmosphere approximation, the airmass model is given by the secant of the zenith angle. The merit scales the strictness of the airmass model using steepness parameter $\alpha$.}
\end{minipage}
\hfill
\begin{minipage}[t]{.49\textwidth}
  \centering
  \includegraphics[width=.75\linewidth]{images/separation_merit}
  \caption{\label{fig:separation}\small Separation angles merit allows for softer limits as targets approach solar system objects, thus improving the probability of observation.}
\end{minipage}
\end{figure}

For a Southern hemisphere observatory at latitude, $\phi$ and an object with declination $\delta$, the minimum and maximum elevation angles are calculated:
\[
E_{\mathit{min}}(\mathit{horizon}) = \left\{
\begin{array}{l}
\ang{-90}-(\phi-\delta),\, \mbox{if transit during observation period}\\
\min\{E(N_{\mathit{start}}), E(N_{\mathit{end}})\}, \, \mbox{otherwise}
\end{array}
\right . \\, \,
\]
\[
E_{max}(horizon) = \left\{
\begin{array}{l}
\ang{90}+(\phi-\delta),\, \mbox{if transit during observation period}\\
\max\{E(N_{\mathit{start}}), E(N_{\mathit{end}})\} \, \mbox{otherwise}
\end{array}
\right . \,
\]


A positional merit can thus be defined as Equation~\ref{eq:position} and shown in Figure~\ref{fig:position}.
\begin{equation}
\label{eq:position}
\varepsilon(\mathit{altitude}) = \frac{h - \max\{E_{\mathit{min}}(\mathit{horizon}), E_{\mathit{min}}(\mathit{limits})\}}{\min\{E_{\mathit{max}}(\mathit{limits})\} - \max\{E_{\mathit{min}}(\mathit{horizon}), E_{\mathit{min}}(\mathit{limits})\}} \, ,
\end{equation}
where $h$ is the current altitude of the target; $E_{\mathit{min}}(\mathit{horizon})$ the minimum pointing angle and $E(\mathit{limits})$ some instrument specific pointing restrictions.
\begin{figure}[h]
\centering
\begin{minipage}[t]{.49\textwidth}
  \centering
  \vspace{0pt} % forced reference point 
  \includegraphics[width=.75\linewidth]{images/position_merit}
\end{minipage}
\hfill
\begin{minipage}[t]{.49\textwidth}
  \centering
  % this image is slightly smaller than the other -- empircally tweak size & spacing
  \vspace{1pt} % forced reference point
  \includegraphics[width=.757\linewidth]{images/target_window_boundary}
\end{minipage}

% top-align captions
\begin{minipage}[t]{.49\textwidth}
  \caption{\label{fig:position}\small The current merit favours higher sky location during positional merit evaluation.}
\end{minipage}
\hfill
\begin{minipage}[t]{.49\textwidth}
  \centering
  \caption{\label{fig:window}\small Merit increases as the fractional time remaining of the target observation window decreases.}
\end{minipage}
\end{figure}

In addition to the target altitude, Figure~\ref{fig:window} shows a window merit that increases the selection weight as the target observation window shortens~\cite{Granzer2004}.
\begin{equation}
\varepsilon(\mathit{window}) = -a\times t_r + \left(\frac{b}{1+c\times t_r}\right)\, ,
\end{equation}
where $t_r = \frac{\Delta t_{\mathit{target}}}{\Delta t_{\mathit{visible}}}$ is the ratio of the target observation window over observation time remaining. The parameters $a,b,c$ in this merit are only used to control the steepness of the rise.

To describe preferences related to the observation target's rise and set times, the start and terminate merits---shown in Figure~\ref{fig:boundary}---controls the rank evaluation as these boundaries approach.
\begin{equation}
\varepsilon(\mathit{boundary}) = \tanh\left(c\times\frac{\Delta t}{\sigma}\right)\, ,
\end{equation}
relaxed by the gradient $\sigma$ as time approaches the start and terminate boundary and $\Delta t$ some buffer around the rise and set times.
\begin{figure}[h]
\centering
\begin{minipage}[t]{.49\textwidth}
  \centering
  \includegraphics[width=.75\linewidth]{images/rise_boundary}
\end{minipage}
\hfill
\begin{minipage}[t]{.49\textwidth}
  \centering
  \includegraphics[width=.75\linewidth]{images/set_boundary}
\end{minipage}
\caption{\label{fig:boundary}\small (Left) Merit indicating strictness to start observation at rise time. (Right) Merit indicating strictness to end observation before set time.}
\end{figure}

\section{Summary}
Planning and scheduling generally refer to off-line processing, while observing requires good solutions with minimal computation time. For each of these stages, we can define the problem input as consisting of the set of observations that have been requested, the constraints peculiar to the instrument\slash environment, and the optimisation of the objective function~\cite{Denny2004}.

Maximum science efficiency is achieved by executing the programmes with highest scientific value first, under the required observing conditions. Additionally, maximised scientific use of telescope time is obtained by having appropriate programmes ready for execution under a broad range of observing conditions.

Fuzzy choices are essential for astronomical observation scheduling and is achieved by representing constraints as merit functions with a strictness parameter associated to each.

\section*{References}
\bibliography{saip}

\end{document}